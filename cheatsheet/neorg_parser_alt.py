#!/usr/bin/python3


def convert_norg_to_html(input_file, output_file):
    try:
        with open(input_file, 'r', encoding='utf-8') as f:
            norg_content = f.readlines()

        with open(output_file, 'w', encoding='utf-8') as f:
            f.write("<!DOCTYPE html>\n<html>\n<head>\n<title>Neorg to HTML</title>\n</head>\n<body>\n")

            # Convert each line to HTML paragraph
            for line in norg_content:
                f.write(f"<p>{line.strip()}</p>\n")

            f.write("</body>\n</html>")

        print(f"Conversion successful. HTML file saved to {output_file}")

    except Exception as e:
        print(f"Error: {e}")

if __name__ == "__main__":
    input_filename = "cheatsheet.norg"
    output_filename = "html/cheatsheet.html"

    convert_norg_to_html(input_filename, output_filename)

