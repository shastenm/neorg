* Song To Midi Conversion

** End Goal
   My overarching objective is to develop an AI program capable of analyzing thousands of songs to predict potential drum and bass lines. The initial concept involves creating an intelligent system that listens to and analyzes songs, extracting information such as BPM (beats per minute), key, musical style, chord progression, and other relevant factors. This analytical framework will serve as the foundation for drawing insights and patterns from the vast pool of songs.

** Potential Libraries To Check Out
*** Aubio
    - Website - https://aubio.org/
    - Github - https://github.com/aubio/aubio 

*** MIDI
   - MIDI++ - https://github.com/jcelerier/libremidi
   - midifile - https://github.com/craigsapp/midifile
   - midi-files - https://github.com/topics/midi-files
   - libremidi - https://github.com/jcelerier/libremidi
   - modernMIDI - https://github.com/ddiakopoulos/modern-midi
   - midi-parser - https://github.com/topics/midi-parser
   - midi-events - https://github.com/topics/midi-events?l=c%2B%2B

*** Essentia (C++, Python)
    - Melody detection - https://essentia.upf.edu/tutorial_pitch_melody.html
    - Essentia examples - https://essentia.upf.edu/essentia_python_examples.html
    - Audio Analysis Library [download] - https://www.researchgate.net/publication/256104772_ESSENTIA_an_Audio_Analysis_Library_for_Music_Information_Retrieval 
    - Sound Processing Topics - https://github.com/topics/sound-processing?utf8=%E2%9C%93&after=Y3Vyc29yOjMw
    - Music Information Retrieval Topic - https://github.com/topics/music-information-retrieval?o=desc&s=updated
    - Audio Analysis Topics - https://github.com/topics/audio-analysis

*** Vamp Plugins
    - Vamp Plugins Website - https://www.vamp-plugins.org/
    - nlls-chroma - https://github.com/c4dm/nnls-chroma
    - vamp-aubio-plugins - https://github.com/aubio/vamp-aubio-plugins/blob/master/vamp-aubio.n3

*** FluidSynth - Generate Midi Sounds (possible prediction model) (C++)
    - FluidSynth Github - https://github.com/FluidSynth/fluidsynth 
    - Topics - https://github.com/topics/fluidsynth 
    - Fork - https://github.com/PatrickCSFan/fluidsynth-fork 

*** PortAudio - recording and playback (used in tandem with analyzing data)
    - Github - https://github.com/PortAudio/PortAudio (C)
    - WIKI - https://github.com/PortAudio/portaudio/wiki/UsingTheGitRepository
    - Topics - https://github.com/topics/portaudio?l=c&o=asc&s=forks

*** libsndfile - C library for reading and writing sound files 
    - Github - https://github.com/libsndfile/libsndfile
    - Projects - https://github.com/libsndfile
    - Tools - https://github.com/libsndfile/sndfile-tools
    - Website - https://libsndfile.github.io/sndfile-tools/

*** MARSYAS - framework for rapid pototyping of audio applications with flexibility and extensibility in mind.
    - Github - https://github.com/marsyas/marsyas 
    - Projects - https://github.com/marsyas 
    - Programming Tutorial - https://www.youtube.com/watch?v=urNdsVT7V3A
    - Implementation - https://github.com/marsyas/marsyas/blob/master/src/marsyas_python/fir_frequency 

*** librosa
    Github - https://github.com/librosa/librosa
    Documentation - https://librosa.org/doc/latest/index.html




