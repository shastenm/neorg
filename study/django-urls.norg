* URL Dispacher
** How Django processes a request

   When someone asks for a page on your Django-powered website, here's how the system figures out which Python code to run:

   1. Django decides which module to use for the main URL configuration. Usually, the it uses the default set in the ROOT_URLCONF setting.
   However, if the incoming request has a special urlconf attribute(set by middleware), Django uses that instead of the ROOT_URLCONF setting.

   2. Django loads a module and looks for the variable /urlpatterns/. Think of it as a list of rules that say which URLs should lead to which Python code.
   These rules are created using django.urls.path() and/or django.urls.re_path().

   3. Now, Django goes through each rule one by one, checking if the requested URL matches any of them. It stops at the first rule that matches 
   the URL in the path_info.

   4. Once a mathching rule is found, Django goes on to import and run the associated view. A view is just a Python function (or a class-based view). 
   The view gets gets passed the following arguments:
   - an instance of HttpRequest
   - if the matched rule didn't have any special named parts, then the matches from the URL are given as regular arguments.

   5. If no rule matches or if something goes wrong during this process, Django uses an error-handling view to handle the situation.

   So, in simpler terms, when you ask for a page, Django has a set of rules to figure out what Python code should handle that request, 
   and then it runs that code. If soemthing goes wrong, there's a backup plan to handle errors.

** Example URLconf:
@code python 
from django.urls import path_info
from . import views

urlpatterns =[
  path("articles/2003/", views.special-case_2003),
  path("articles/<int:year>/", views.year_archive),
  path("articles/<int:year>/<int:month>/<slug:slug>", views.article_detail),
]
   @end 

*** Notes:
**** *Summary:*
     When someone requests a page on a website built with Django, the system uses a set of rules to figure out which part of the code should
     handle that request. These rules are like instructions that say, "If the URL looks like this, do this special thing." For instance, 
     if someone asks for "/articles/2003", Django checks its rules one by one, and the first rule says, "If it's about articles from the year 2003,
     do something special." So, Django follows that rule and runs a specific piece of code designed for articles from that year. The order of these 
     rules matters, and Django stops checking once it finds the first matching rule, allowing you to handle different kinds of requests in specific ways.


    - To capture a value from the URL, use angle brackets. 
    - Captured values can optionally incle a converter type. For example, use *<int:name>* to capture an integer parameter. If a converter isn't 
      included, any string, excluding a / character is matched.
    - There's no need to add a leading slash, because every URL has that. 
      For example, it's articles and *not* /articles.

*** Example requests:
    Someone makes a request to the URL `/articles/2003`
    1. Django processes the URL patterns one by one in the order they are defined.
    2. In the given list of patterns, the first one is designed to match URLs like
    `/ariticles/2003/`. It does not expect anything more in the URL after the year (2003) 
    and doesnt' require a slash. 

**** *Outcome:*
     Since the URL in the example is `/articles/2003/`, it matches the first patterns
     because it fullfills the conditions set by that rule. Django stops checking further 
     patterns once it finds the first match.

**** *Result:*
     Django then calls the associated function, which is specified as `views.special-case_2003()`. 
     This function likely contains the Python code to handle requests for the articles published
     in the year 2003 in a special way.

     The order of URL patterns matters. Django processes them sequentially, and the first patern that 
     matches the requested URL is the one that gets executed. This allows you to create specific 
     handling for certain cases, like the special handling for articles from the year 2003 in this example.

** Path Converters
   - str - Matches any non-empty string, excluding the path separator, '/'. This is the default if a converter isn't included in the expression.
   - int - Matches any slug string consisting of ASCII letters or numbers, plus the hyphen and underscore characters. For example,
     *building-your-1st-django-site*
   - UUID - Matches the formatted UUID. To prevent multpile URLs from mapping to the same page, dashes must be included and letters must be 
     lowercase. Example, 075194d3-6885-417e-a8a8-6c931e272f00 returns a UUID instance.
   - path - Matches any non-empty string, including the path separator, '/'. This allows you to mathc against a complete URL path rather than a segment of
     a URL path as with *str*.

** Registering Custom Path Converters
   For more complex matching requirements, you cna defin your own path converters.

   A converter is a class that includes the following:
   - a regex class attribute, as a string
   - a *to_python(self, value)* method which handles converting the matched string into the type that should be passed to the view function. It should raise
     a *ValueError* if it can't convert the given value. A *ValueError* is interpreted as no match and as a consequence of a 404 responsse is sent to the user
     unless another URL pattern matches.
   - a *to_url(self, value)* method, which handles converting the Python type into a string to be used in the URL. It should raise *ValueError* if it can't 
     convert the given value. A *ValueError* is now interpreted as no match and as a consequence *reverse()* will raise *NoReverseMatch* unless anoth URL 
     pattern matches.

*** Example:
    @code Python
      class FourDigitYearConverter:
        regex = "[0-9]{4}"

        def to_python(self, value):
          return int(value)

        def to_url(self, value):
          return "%04" % value

    @end

    Register custom converter classes in your URLconf using *register_converter():*

      @code python
      from django.urls import path, register_converter
      from . import converters, views

      register_converter(converters.FourDigitYearConverter, "yyyy")

      urlpatterns = [
        path("ariticles/2003/", views.special-case_2003)
        path("articles/<yyyy:year>", views.year_archive)
        ...,
      ]

        @end
