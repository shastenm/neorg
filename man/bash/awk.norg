* awk (GNU Awk)

* *Description*

- The awk command is a versatile tool for finding and replacing text, as well as sorting, validating, and indexing data in files. 
  It operates on patterns and actions, executing specific actions when patterns are matched in the input data.

  @code bash
  awk <options> 'Program' Input-File1 Input-File2 ...
  awk -f PROGRAM-FILE <options> Input-File1 Input-File2 ...
  @end

* Options
  - -F FS, --field-separator FS: Use FS as the input field separator.
  - -f PROGRAM-FILE, --file PROGRAM-FILE: Read the awk program source from the file PROGRAM-FILE.
  - -mf NNN, -mr NNN: Set the maximum number of fields (f) and maximum record size (r).
  - -v VAR=VAL, --assign VAR=VAL: Assign the variable VAR the value VAL before program execution.
  - -W traditional, -W compat, --traditional, --compat: Use compatibility mode with gawk extensions turned off.
  - -W lint, --lint: Give warnings about dubious or non-portable awk constructs.
  - -W lint-old, --lint-old: Warn about constructs not available in the original V.7 Unix version of awk.
  - -W posix, --posix: Use POSIX compatibility mode, turning off gawk extensions with additional restrictions.
  - -W re-interval, --re-interval: Allow interval expressions in regexps.
  - -W source=PROGRAM-TEXT, --source PROGRAM-TEXT: Use PROGRAM-TEXT as awk program source code.
  - --: Signal the end of options.
  - 'Program': A series of patterns and actions.
  - Input-File: The file(s) to process. If none specified, awk applies the program to standard input.

* Basic Function
- The primary function of awk is to search files for lines containing a specified pattern and perform a specific action on matching lines.

* Example
- Suppose we have a file with names and phone numbers. To retrieve Audrey's phone number:
  @code bash
    awk '$1 == "Audrey" {print $2}' numbers.txt
  @end

- This means if the first field matches "Audrey," then print the second field.

- In awk, $0 represents the whole line, and $1, $2, etc., represent individual fields.

  For readability, each line in an awk program is typically a separate Program statement:
  
  @code bash
    pattern { action }
    pattern { action }
    ...
  @end

Awk Patterns
  - /Regular Expression/: Match a regular expression.
  - Pattern && Pattern: AND.
  - Pattern || Pattern: OR.
  - ! Pattern: NOT.
  - Pattern ? Pattern : Pattern: If, Then, Else.
  - Pattern1, Pattern2: Range Start - end.
  - BEGIN: Perform action before input file is read.
  - END: Perform action after input file is read.

* Variables
** Some special variable names in awk:

   - CONVFMT: Conversion format used when converting numbers.
   - FS: Regular expression separating fields.
   - NF: Number of fields in the current record.
   - NR: Ordinal number of the current record.
   - FNR: Ordinal number of the current record in the current file.
   - FILENAME: Name of the current input file.
   - RS: Input record separator.
   - OFS: Output field separator.
   - ORS: Output record separator.
   - OFMT: Output format for numbers.
   - SUBSEP: Separates multiple subscripts.
   - ARGC: Argument count.
   - ARGV: Argument array.
   
* Examples
  @code bash

  # Print the fifth item from each line of an ls -l listing:
  $ ls -l | awk '{print $5}'

  # Print the Row Number (NR) and the first item from each line in samplefile.txt:
  $ awk '{print NR "- " $1 }' samplefile.txt

  # Delete blank lines from a file:
  $ awk 'NF > 0' data.txt 

  # Print the length of the longest input line:
  $ awk '{ if (length($0) > max) max = length($0) }
    END { print max }' data

  # Print seven random numbers from 0 to 100:
  $ awk 'BEGIN { for (i = 1; i <= 7; i++)
              print int(101 * rand()) }'

  # Print a sorted list of login names from /etc/passwd:
  $ awk -F: '{ print $1 }' /etc/passwd | sort 

  # Count the lines in a file:
  $ awk 'END { print NR }' @data

  # Print even-numbered lines in the data file:
  $ awk 'NR % 2 == 0' data

  @end

Awk is a powerful tool for text processing and data manipulation.
